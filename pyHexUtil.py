from binascii import unhexlify as unhex
from binascii import hexlify 
import re

def raw(input):
	return re.sub('[^0-9A-Fa-f]','',input)

def scatter(input):
	return re.sub(r' $','',re.sub(r'([0-9A-Fa-f]{2})',r'\1 ',input))

def dehex(input):
	return hexlify(input).decode('ascii').upper()

def coslen(input):
	input=raw(input)
	return len(input)/2

def cossub(input,i=0,j=0):
	input=raw(input)
	if i>j:
		j=i
	i-=1
	return input[2*i:2*j]


