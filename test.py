from pyPBOC import *
from pyReader import *


'''test pydes'''
'''des'''


print('enctypt result:\t', PbocDes("FF" * 8).encrypt("11" * 16))
print('dectypt result:\t', PbocDes("FF" * 8).decrypt("11" * 16))
print('key:\t', PbocDes("FF" * 8).key())
print('diverse key:\t', PbocDes("FF" * 8).diverse('1122334455667788').key())
print('diverse 2 levels key:\t', PbocDes(
    "FF" * 8).diverse('1122334455667788').diverse('8877665544332211').key())
print('computer mac:\t', PbocDes("FF" * 8).mac("22" * 17))
print('computer mac with IV:\t', PbocDes(
    "FF" * 8).mac("22" * 17, "0000000000000001"))

print('---------------------')
'''3des'''

print('enctypt result:\t', Pboc3Des("EE" * 16).encrypt("11" * 16))
print('dectypt result:\t', Pboc3Des("EE" * 16).decrypt("11" * 16))
print('key:\t', Pboc3Des("EE" * 16).key())
print('diverse key:\t', Pboc3Des("EE" * 16).diverse('1122334455667788').key())
print('diverse 2 levels key:\t',
      Pboc3Des("EE" * 16)
      .diverse('1122334455667788')
      .diverse('8877665544332211')
      .key()
      )
print('computer mac:\t', Pboc3Des("EE" * 16).mac("22" * 17))
print('computer mac with IV:\t', Pboc3Des(
    "EE" * 16).mac("22" * 17, "0000000000000001"))

print('diverse 2 levels key and compute mac:\t',
      Pboc3Des("EE" * 16)
      .diverse('1122334455667788')
      .diverse('8877665544332211')
      .mac("22" * 17,"FFFFFFFFFFFFFFFF")
      )



'''test pyreader'''
d8 = Reader()
d8.reset()
err, ats = d8.cpu_detect()
d8.beep()
print('ats:', ats)
d8.set_trace('on')
# d8.set_assert('on')
sw, msg = d8.cpu_send('00A40000021001')
sw, msg = d8.cpu_send("00 B0 95 00 1E")


d8.psam_select(2)
err, ats = d8.psam_detect()
print('psam ats', ats)
sw,msg=d8.psam_send("00A40000023F00")


d8.close()