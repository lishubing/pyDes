from pyHexUtil import *
from ctypes import *
import os

TRACE_OFF = 0
TRACE_ON = 1

ASSERT_OFF = 0
ASSERT_ON = 1


class Reader(object):

    '''D8 reader functions'''

    def __init__(self, port=100, baud=115200):
        super(Reader, self).__init__()
        self._reader = windll.LoadLibrary('./dcrf32.dll')
        self._icdev = self._reader.dc_init(port, baud)
        self._trace = TRACE_ON
        self._assert = ASSERT_OFF
        if self._icdev < 0:

            print('dc_init failed:' + str(self._icdev))
            self.close()
            raise Exception('dcDriverFailed')

    def open(self, port=100, baud=115200):
        self._icdev = self._reader.dc_init(port, baud)
        if self._icdev < 0:
            print('dc_init failed:' + str(self._icdev))
            self.close()
            raise Exception('dcDriverFailed')
        self.beep()

    def reset(self):
        self._reader.dc_reset(self._icdev, 1)

    def close(self):
        self._reader.dc_exit(self._icdev)
        self._icdev = 0

    def set_trace(self, input):
        if input == 'on':
            self._trace = TRACE_ON
        elif input == 'off':
            self._trace = TRACE_OFF

    def set_assert(self, input):
        if input == 'on':
            self._assert = ASSERT_ON
        elif input == 'off':
            self._assert = ASSERT_OFF

    def _print(self, input):
        if self._trace == TRACE_ON:
            print(input)

    def beep(self, sec=10):
        return self._reader.dc_beep(self._icdev, sec)

    def cpu_detect(self):
        tagtype = c_int(0)
        err = self._reader.dc_request(self._icdev, 1, pointer(tagtype))
        if err != 0:
            print('dc_request falied:' + str(err))
            self.close()
            raise Exception("dcDriverFailed")
            return err

        snr = c_ulong(0)
        err = self._reader.dc_anticoll(self._icdev, 0, pointer(snr))
        if err != 0:
            print('dc_anticoll falied:' + str(err))
            self.close()
            raise Exception("dcDriverFailed")
            return err

        size = c_ubyte(0)
        err = self._reader.dc_select(self._icdev, snr, pointer(size))
        if err != 0:
            print('dc_select falied:' + str(err))
            self.close()
            raise Exception("dcDriverFailed")
            return err

        msg = create_string_buffer(256)
        msglen = c_ubyte(0)

        err = self._reader.dc_pro_reset(
            self._icdev, pointer(msglen), pointer(msg))
        if err != 0:
            print('dc_pro_reset falied:' + str(err))
            self.close()
            raise Exception("dcDriverFailed")
            return err
        return err, dehex(msg.value)

    def cpu_send(self, input):
        input = raw(input)
        if len(input) % 2 != 0:
            print('command len error!')
            return ""

        smsg = create_string_buffer(unhex(input), 256)
        slen = c_ubyte(len(unhex(input)))
        rlen = c_ubyte(0)
        rmsg = create_string_buffer(256)
        timeout = c_ubyte(10)
        frag = c_ubyte(80)
        err = c_ulong(0)
        # print(slen.value, dehex(smsg.raw), rlen.value, timeout.value,
        # frag.value)
        self._reader.dc_pro_commandlink.restype = c_ulong
        self._print('CPU <-- :' + scatter(input))
        err = self._reader.dc_pro_commandlink(
            self._icdev, slen, pointer(smsg), pointer(rlen), pointer(rmsg), timeout, frag)
        if rlen.value == 0:
            print('dc_pro_command falied:' + str(err) + str(rlen))
            self.close()
            raise Exception("dcDriverFailed")
            return None

        sw = dehex(rmsg.raw[rlen.value - 2:rlen.value])
        msg = dehex(rmsg.raw[:rlen.value - 2])
        self._print('CPU --> :' + scatter(msg) + '|' + sw)
        if self._assert == ASSERT_ON and (sw != '9000' and sw[:2] != '61'):
            self.close()
            raise Exception("UnexpectedPDU")
        return sw, msg

    def psam_select(self, slot=1, baud=38400):
        if slot > 3 or slot < 1:
            print("Invalid slot")
            return None
        cputype = c_ubyte(slot + 12)
        if baud == 38400:
            cpuetu = c_ubyte(20)
        elif baud == 9600:
            cpuetu = c_ubyte(92)
        else:
            print('Invalid baud')
            return None

        err = self._reader.dc_setcpupara(self._icdev, cputype, 0, cpuetu)
        if err != 0:
            print('dc_setcpupara falied:' + str(err))
            return None

        err = self._reader.dc_setcpu(self._icdev, cputype)
        if err != 0:
            print('dc_setcpu falied:' + str(err))
            return None

    def psam_detect(self):
        atslen = c_ubyte(0)
        ats = create_string_buffer(256)
        err = self._reader.dc_cpureset(
            self._icdev, pointer(atslen), pointer(ats))
        if err != 0:
            print('dc_cpureset falied:' + str(err))
            return None
        return err, dehex(ats.raw[:atslen.value])

    def psam_send(self, input):
        input = raw(input)
        if len(input) % 2 != 0:
            print('command len error!')
            return None

        smsg = create_string_buffer(unhex(input), 256)
        slen = c_ubyte(len(unhex(input)))
        rlen = c_ubyte(0)
        rmsg = create_string_buffer(256)

        # print(slen.value, dehex(smsg.raw), rlen.value, timeout.value, frag.value)

        self._print('PSAM <-- :' + scatter(input))
        err = self._reader.dc_cpuapdu(
            self._icdev, slen, pointer(smsg), pointer(rlen), pointer(rmsg))
        if err != 0:
            print('dc_pro_command falied:' + str(err))
            return None

        sw = dehex(rmsg.raw[rlen.value - 2:rlen.value])
        msg = dehex(rmsg.raw[:rlen.value - 2])
        self._print('PSAM --> :' + scatter(msg) + '|' + sw)
        if self._assert == ASSERT_ON and (sw != '9000' and sw[:2] != '61'):
            raise Exception("UnexpectedPDU")
        return sw, msg
