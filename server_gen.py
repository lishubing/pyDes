
from softkey_credit import credit
from softkey_purchase import purchase
from softkey_fileio import *
import os
import os.path
import random
import string
import time
import cherrypy
import webbrowser

device_in_use = False


class RootService(object):
    @cherrypy.expose
    def index(self):
        return open('./cardutils/index.html', 'r', encoding='utf8', errors='ignore')


class PurseWebService(object):
    exposed = True

    @cherrypy.tools.accept(media='text/plain')
    def PUT(self, biztype, money=0):
        print(cherrypy.request.remote.ip)
        global device_in_use
        print("purse")
        time_start = time.clock()
        try:
            if device_in_use == True:
                ret = '设备正忙'
            elif biztype == 'recharge':
                device_in_use = True
                ret = '充值成功！卡内余额：' + '%.2f' % (credit(int(money)) / 100.0)+ '\n耗时%.2fms' % ((time.clock() - time_start) * 1000)
            elif biztype == 'consume':
                device_in_use = True
                ret = '消费成功！卡内余额：' + '%.2f' % (purchase(int(money)) / 100.0)+ '\n耗时%.2fms' % ((time.clock() - time_start) * 1000)
            else:
                ret = "未知操作"
        except:
            print("System Error!")
            ret = "系统错误"
        device_in_use = False
        return ret


class FileWebService(object):
    exposed = True

    @cherrypy.tools.accept(media='text/plain')
    def PUT(self, biztype, adf, sfi, index, filetype, data=""):
        global device_in_use
        try:
            if device_in_use == True:
                ret = '设备正忙'
            elif biztype == 'read':
                device_in_use = True
                ret = fileread(adf, sfi, index, filetype)
            elif biztype == 'write':
                device_in_use = True
                ret = filewrite(adf, sfi, index, filetype, data)

            else:
                ret = "未知操作"
        except:
            print("System Error!")
            ret = ""
        device_in_use = False
        return ret


if __name__ == '__main__':
    conf = {
        'global': {  
            'server.socket_host': '0.0.0.0',  
            'server.socket_port': 1989,  
            'tools.encode.on':True,   
            'tools.encode.encoding':'utf8',   
        },
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/purse': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/file': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')],
        },
        '/public': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './cardutils/public'
        },
        '/jQueryAssets': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './cardutils/jQueryAssets'
        }
    }
    webapp = RootService()
    webapp.purse = PurseWebService()
    webapp.file = FileWebService()
    cherrypy.quickstart(webapp, '/', conf)
