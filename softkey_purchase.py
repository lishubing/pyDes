from pyReader import *
from pyPBOC import *
from keys import *
import time

def purchase(imoney=0):
    d8 = Reader()
    d8.reset()
    d8.cpu_detect()
    d8.beep()
    d8.cpu_send('00A40000023F00')
    d8.cpu_send("00A40000021001")

    sw, msg = d8.cpu_send("00 B0 95 00 1E")
    card_no = cossub(msg, 13, 20)
    card_key = Pboc3Des(purchase_key).diverse(card_no)

    trade_time = time.strftime('%Y%m%d%H%M%S')
    money = '%08X' % imoney
    terminal_id = '0123456789AB'

    sw, sa_code = d8.cpu_send("80 CA 00 00 09")

    sw, msg = d8.cpu_send("80 50 01 02 0B 01 " + money + terminal_id)

    card_tradesn = cossub(msg, 5, 6)
    card_rand = cossub(msg, 12, 15)
    terminal_tradesn = '00000001'
    sess_key = PbocDes(
        card_key.encrypt(card_rand + card_tradesn + cossub(terminal_tradesn, 3, 4)))

    mac = sess_key.mac(money + '06' + terminal_id + trade_time + sa_code)

    sw, msg = d8.cpu_send('805401000F' + terminal_tradesn + trade_time + mac)

    sw, msg = d8.cpu_send("80 5C 00 02 04")
    print("success, balance is " + msg)
    print('card_tradesn', card_tradesn)
    d8.cpu_send("805A000602" + '%04X' % (int(card_tradesn, 16) + 1) + "08")
    d8.close()
    return int(msg,16)
