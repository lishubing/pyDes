from pyReader import *


d8 = Reader()

d8.reset()
err, ats = d8.cpu_detect()
d8.beep()
print('ats:', ats)
d8.set_trace('on')
# d8.set_assert('on')

sw, sa_code = d8.cpu_send("80CA000009")


d8.psam_select(2)
err, ats = d8.psam_detect()
print('psam ats', ats)
sw,msg=d8.psam_send("00A40000023F00")
sw,msg=d8.psam_send("00A40000021004")
sw, msg = d8.cpu_send("80CA000009"+sa_code)
d8.close()