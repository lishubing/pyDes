from pyReader import *
from pyPBOC import *
from keys import *
from softkey_fileio import *
from softkey_credit import *
from softkey_purchase import *
adf_sfi = {'ADF1': '1001', 'ADF2': '1002',
           'ADF3': '1003', 'ADF4': '1004', 'MF': '3F00'}

key_00 = '00' * 16
key_ff = 'FF' * 16

def replace_key(dack,key_input,key_type,key_index,rand):
    root_key = Pboc3Des(dack)
    rand += '00000000'
    cmd = '84D4' + key_type + key_index + '1C' + root_key.encrypt('10'+key_input+'80000000000000')
    cmd+=root_key.mac(cmd,rand)
    return cmd





card_no = '0201000000002541'
local_manage_key=Pboc3Des(manage_key).diverse(card_no).key()
local_purchase_key=Pboc3Des(purchase_key).diverse(card_no).key()
local_credit_key=Pboc3Des(credit_key).diverse(card_no).key()
file05='00 01 00 00 00 03 00 00 '+card_no+' 00 01 20 14 11 11 00 00 00 00 00 03 00 01'
file15='00 01 11 00 00 03 00 00 03 01 11 00 '+card_no+' 20 14 11 11 20 24 11 11 00 00'
file1901='01 40 03 08 00 01 00 03 00 00 00 00 20 15 06 13 17 40 42 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00'



d8 = Reader()
d8.reset()
d8.beep()
d8.cpu_detect()
d8.set_assert('on')

d8.cpu_send('00A40000023F00')
'''
#relace manage key
sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(replace_key(
        key_00,
        key_00,
        '36','00',
        rand
        ))
'''
sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(filewrite_cmd(key_00, '05', '00', rand, 'bin', file05))

d8.cpu_send('00A40000021001')


'''relace manage key'''
sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(replace_key(
        key_00,
        local_manage_key,
        '36','00',
        rand
        ))


'''relace purchase key'''
sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(replace_key(
        key_00,
        local_purchase_key,
        '27','01',
        rand
        ))
sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(replace_key(
        key_00,
        local_purchase_key,
        '27','02',
        rand
        ))

'''relace credit key'''
sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(replace_key(
        key_00,
        local_credit_key,
        '26','01',
        rand
        ))
sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(replace_key(
        key_00,
        local_credit_key,
        '26','02',
        rand
        ))


sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(filewrite_cmd(local_manage_key, '15', '00', rand, 'bin', file15))



sw, rand = d8.cpu_send('0084000004')
d8.cpu_send(filewrite_cmd(local_manage_key, '19', '01', rand, 'rec', file1901))

credit(1000)
#purchase(1000)