from pyReader import *
from pyPBOC import *
from keys import *

adf_sfi = {'ADF1': '1001', 'ADF2': '1002',
           'ADF3': '1003', 'ADF4': '1004', 'MF': '3F00'}




def fileread(adf, sfi, index='01', filetype='bin'):
    d8 = Reader()
    d8.reset()
    d8.beep()
    d8.cpu_detect()
    d8.set_assert('off')
    d8.cpu_send('00A4000002' + adf_sfi[adf])
    
    cmd = '00'
    if filetype == 'bin':
        cmd += 'B0' + '%02X' % (int(sfi, 16) | 0x80) + '0000'
    else:
        cmd += 'B2'+index+'%02X' % ((int(sfi, 16)<<3 )| 0x04)+'00'
    sw, content=d8.cpu_send(cmd)
    if sw!='9000':
        return sw
    d8.close()
    return content

def filewrite(adf,sfi, index='00', filetype='bin', data=""):
    data=raw(data)

    file15=fileread('ADF1','15','00','bin')
    card_no = cossub(file15, 13, 20)
    card_key = Pboc3Des(manage_key).diverse(card_no)
    
    d8 = Reader()
    d8.reset()
    d8.cpu_detect()
    d8.set_assert('off')
    
    
    d8.cpu_send('00A4000002' + adf_sfi[adf])
    sw,rand = d8.cpu_send("0084 0000 04")
    rand+='00000000'


    cmd = '04'
    if filetype == 'bin':
        cmd += 'D6' + '%02X' % (int(sfi, 16) | 0x80) + '00'+'%02X'%(coslen(data)+4)+data
    else:
        cmd += 'DC'+index+'%02X' % ((int(sfi, 16)<<3 )| 0x04)+'%02X'%(coslen(data)+4)+data
    cmd+=card_key.mac(cmd,rand)

    sw, content=d8.cpu_send(cmd)
    d8.close()
    return sw


def filewrite_cmd(card_key,sfi, index, rand,filetype='bin', data=""):
    data=raw(data)   
    rand+='00000000'
    cmd = '04'
    if filetype == 'bin':
        cmd += 'D6' + '%02X' % (int(sfi, 16) | 0x80) + '00'+'%02X'%(coslen(data)+4)+data
    else:
        cmd += 'DC'+index+'%02X' % ((int(sfi, 16)<<3 )| 0x04)+'%02X'%(coslen(data)+4)+data
    cmd+=Pboc3Des(card_key).mac(cmd,rand)
    return cmd