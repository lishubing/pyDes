from pyDes import *
import re
from pyHexUtil import *


def inverse(input):
    return bytes(list(map(lambda x: (~x) & 0xff, bytes(input))))


def bytexor(input1, input2):
    return bytes(list(map(lambda x, y: x & 0xff ^ y & 0xff, bytes(input1), bytes(input2))))


def padding(input):
    return input + "8000000000000000"[:2 * int(8 - (len(input) / 2) % 8)]


class __PbocCrypt(object):

    """the base class of des and 3des crypt"""

    def __init__(self, key):
        key = raw(key)
        if(len(key) % 16 != 0):
            raise Exception('InvalidKeyLenth')
        self._result = None

    def encrypt(self, input=None):
        input = raw(input)
        if input == None:
            input = self._result
        self._result = dehex(self._crypt.encrypt(unhex(input)))
        return self._result

    def decrypt(self, input=None):
        input = raw(input)
        if input == None:
            input = self._result
        self._result = dehex(self._crypt.decrypt(unhex(input)))
        return self._result

    def diverse(self, div):
        '''diverse the key, and return the crypt object'''
        div = raw(div)
        self._crypt.setKey(
            unhex(self.encrypt(div) + self.encrypt(dehex(inverse(unhex(div))))))
        return self

    def key(self):
        return dehex(self._crypt.getKey())

    def result(self):
        # legacy function, don't use it
        return self._result

    def mac(self, data, iv='0000000000000000'):
        iv = raw(iv)
        data = unhex(padding(raw(data)))

        keyleft = self._crypt.getKey()[:8]
        keyright = self._crypt.getKey()[-8:]

        data = des(keyleft, CBC, unhex(iv)).encrypt(data)

        if len(self._crypt.getKey()) == 16:
            data = des(keyright).decrypt(data)
            data = des(keyleft).encrypt(data)

        return dehex(data[-8:-4])


class PbocDes(__PbocCrypt):

    """des crypt"""

    def __init__(self, key="F" * 32, mode=ECB, iv="0000000000000000"):
        super(PbocDes, self).__init__(key)
        self._crypt = des(unhex(key), mode, unhex(iv))


class Pboc3Des(__PbocCrypt):

    """3des crypt"""

    def __init__(self, key="F" * 32, mode=ECB, iv="0000000000000000"):
        super(Pboc3Des, self).__init__(key)
        self._crypt = triple_des(unhex(key), mode, unhex(iv))
